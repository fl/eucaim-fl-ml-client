#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import warnings
import flwr as fl
import numpy as np

from pathlib import Path
from logging import INFO, DEBUG
from flwr.common.logger import log
from typing import Tuple, Union, List
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import LogisticRegression, LinearRegression


data_dir = os.getenv( 'DATA_PATH' )   
#data_dir = '/home/carles/Downloads/chest_xray/1'
#n_client = '1'


def get_model_parameters( model ):
    if model.fit_intercept:
        params = [
            model.coef_,
            model.intercept_,
        ]
    else:
        params = [
            model.coef_,
        ]
    return params


def set_model_params( model, params ):
    model.coef_ = params[ 0 ]
    if model.fit_intercept:
        model.intercept_ = params[ 1 ]
    return model


def set_initial_params( model ):
    model.coef_ = np.zeros( ( 1, 4 ) )
    if model.fit_intercept:
        model.intercept_ = np.zeros( ( 1, ) )


def load_data( path ):
    data_file = data_dir + '/z3.csv'
    Xy = np.genfromtxt( data_file, delimiter = ',', dtype = float )

    X = np.array( Xy[ 2:, :-1 ], dtype = float )
    y = np.array( Xy[ 2:,  -1 ], dtype = float )

    x_train, y_train = X[ :22 ], y[ :22 ]
    x_test,  y_test  = X[ 22: ], y[ 22: ]

    return (x_train, y_train), (x_test, y_test)


def shuffle( X, y):
    """Shuffle X and y."""
    rng = np.random.default_rng()
    idx = rng.permutation(len(X))
    return X[idx], y[idx]


def partition( X, y, num_partitions ):
    return list(
        zip( np.array_split( X, num_partitions ), np.array_split( y, num_partitions ) )
    )

class LogitClient( fl.client.NumPyClient ):
    def get_parameters( self, config ):
        return get_model_parameters( model )

    def fit( self, parameters, config ):
        set_model_params( model, parameters )
        with warnings.catch_warnings():
            warnings.simplefilter( 'ignore' )
            model.fit( X_train, y_train )
        print( f"Training finished for round {config['server_round']}" )
        return get_model_parameters( model ), len( X_train ), {}

    def evaluate(self, parameters, config):
        set_model_params( model, parameters )
        mse = mean_squared_error( model.predict( X_test ), y_test )
        accuracy = model.score( X_test, y_test )
        return mse, len( X_test ), { 'accuracy': accuracy }


model = LinearRegression()

if __name__ == "__main__":
    log( DEBUG, 'FLOWER_SSL_CACERT : {}'.format( os.getenv( 'FLOWER_SSL_CACERT' ) ) )
    log( DEBUG, 'DATA_PATH : {}'.format( os.getenv( 'DATA_PATH' ) ) )
    log( DEBUG, 'FLOWER_CENTRAL_SERVER : {}:{}'.format( os.getenv( 'FLOWER_CENTRAL_SERVER_IP' ), os.getenv( 'FLOWER_CENTRAL_SERVER_PORT' ) ) )
    log( DEBUG, 'QUICK_RUN  : {}'.format( os.getenv( 'QUICK_RUN' ) ) )
    
    ( X_train, y_train ), ( X_test, y_test ) = load_data( data_dir )

    partition_id = np.random.choice( 10 )
    ( X_train, y_train ) = partition( X_train, y_train, 10 )[ partition_id ]


    set_initial_params( model )

    if os.getenv( 'NOSECURE' ) is not None:
        log( DEBUG, 'NOSECURE : {}'.format( os.getenv( 'NOSECURE' ) ) )
        fl.client.start_numpy_client( 
            server_address = "{}:{}".format( os.getenv( 'FLOWER_CENTRAL_SERVER_IP' ), os.getenv( 'FLOWER_CENTRAL_SERVER_PORT' ) ), 
            client = LogitClient(),
            root_certificates = None
        )
    else:
        fl.client.start_numpy_client( 
            server_address = "{}:{}".format( os.getenv( 'FLOWER_CENTRAL_SERVER_IP' ), os.getenv( 'FLOWER_CENTRAL_SERVER_PORT' ) ), 
            client = LogitClient(),
            root_certificates = Path( os.getenv( 'FLOWER_SSL_CACERT' ) ).read_bytes()
        )
