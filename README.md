# EUCAIM FL ML Client


## Introduction

This repository contains the `Dockerfile` and the `client.py` for the client to be used in the [EUCAIM-WP6's demonstration](https://github.com/EUCAIM/fl_demonstrator) for Federated Learning using learning models (aka. logistic regression).

## To be run

This client is to be run with its partner server located [here](https://gitlab.bsc.es/fl/eucaim-fl-ml-server). The orchestration of the experiment is to be done using the [FLmanager](https://gitlab.bsc.es/fl/FLManager).

## Data

According to the description of of the [traditional machine learning challenge](https://github.com/EUCAIM/demo_ml_data/), the data is obtained from [this R package](https://www.bioconductor.org/packages/release/bioc/html/rexposome.html). The challenge description includes the files already processed for the [three data-sires scenario](https://github.com/EUCAIM/demo_ml_data/tree/main/data/three_dataseties_scenario).

   * Download one of the files according to your data-site (aka, `z3_[data-site].csv` for `data-site` as `1`, `2`, or `3`).
   * Rename the file to `z3.csv`
