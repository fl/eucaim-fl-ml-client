FROM ubuntu:22.04
RUN apt update
RUN apt install -y python3-pip git

ARG DEBIAN_FRONTEND=noninteractive

WORKDIR "/eucaim/docker"

# set bash as current shell
RUN chsh -s /bin/bash
SHELL ["/bin/bash", "-c"]
RUN pwd

#COPY . /eucaim/docker
ADD client.py .
ADD requirements.txt .
RUN pip3 install -r requirements.txt
ENV PYTHONPATH "${PYTHONPATH}:/eucaim"

WORKDIR "/flcore"

# run the client
# CMD [ "python3", "client.py" ]

